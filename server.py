import flask
from flask import Flask, abort,request,jsonify
from markupsafe import escape
import json

app = Flask(__name__)

länder_infos_a = open("datenbank_backup.json","r+")
länder_infos_a.encoding.encode("utf-8")
länder_infos = json.load(länder_infos_a)
json.dump(länder_infos,open("datenbank.json","w+"))


@app.route('/<land_param>', methods=["GET"])
def land_infos_handler(land_param):
    print(request.headers.get("User-Agent"))
    if länder_infos.get(escape(land_param)) is None: abort(404)

    response = jsonify(länder_infos.get(land_param))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route("/<land_param>", methods=["POST"])
def post(land_param):
    global länder_infos
    if länder_infos.get(escape(land_param)) is None:
        länder_infos.update({land_param:{}})

    payload = dict(request.form)

    for info in payload.items():
        länder_infos[escape(land_param)].update({info[0]:info[1]})

    json.dump(länder_infos, open("datenbank.json", "w+"))

    response = jsonify(länder_infos[escape(land_param)])
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route("/<land_param>/<attribute>")
def land_info_handler(land_param, attribute):
    if länder_infos.get(escape(land_param)) is None: abort(404)
    land_infos = länder_infos.get(land_param)

    if land_infos.get(attribute) is None: abort(404)

    response = jsonify(land_infos.get(attribute))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route("/infos")
def länder_handler():
    response = jsonify(länder_infos)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response



@app.route("/")
def main():
    response = jsonify(list(länder_infos.keys()))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route("/status")
def is_running():
    response = jsonify("running")
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.after_request
def after_request(response:flask.Response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Methods"] = "GET, POST, PUT, DELETE, OPTIONS"
    response.headers["Access-Control-Allow-Headers"] = "Content-Type, X-Requested-With"
    response.access_control_allow_headers = "*"

    return response


app.config['CORS_HEADERS'] = 'Content-Type'
app.config['JSON_AS_ASCII'] = False

app.run(host="0.0.0.0", port=5000)
