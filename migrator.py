import json

import pandas


def format(table):
    rows = len(table.values)
    cols = len(table.columns.values)

    newtable = {}
    for i in range(rows):
        key = table.iat[i, 0]
        value = {table.columns.values[x + 1]: table.iat[i, x + 1] for x in range(cols - 1)}
        newtable.update({key: value})
    return newtable

def unformat(table):
    newtable = []
    for elem in table.items():
        row = tuple(elem)

        row = [row[0]]+[value for key,value in row[1].items()]
        newtable.append(row)
    print(newtable)
    print()
    newtable = pandas.DataFrame(newtable, columns=[pandas.read_csv("./datenbank.csv").columns])
    newtable.reset_index(drop=True)
    newtable.set_index("Land")
    print(newtable)
    return newtable

table = pandas.read_csv("[FILE].csv")
länder_infos = format(table)
json.dump(länder_infos,open("[FILE].json","w+"))