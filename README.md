[Link to frontend of CultureGuide](https://gitlab.com/jugendhackt/culture-google-frontend)

# Länder Api Dokumentation

## Benutzen der Api:

### IP: http://192.168.178.42:5000

### 1. Abfragbare Länder auflisten

- **[IP]**
- **Returnt:**
  - **["LAND","LAND 2"]**

### 2. Infos zu Ländern erhalten

- **[IP]/[LAND]**
- **Returnt:**
  - **{"Kategorie":"Antwort"}**

### 3. Infos zu allen Ländern erhalten (So wie 2 nur für alle)

- **[IP]/infos**
- **Returnt:**
  - **["LAND":{"Kategorie":"Antwort"}]**

### 4. Eine einzelne Info zu einem Land erhalten

- **[IP]/[LAND]/[ATTRIBUT]**
- **Returnt:**
  - **"ATTRIBUT"**

### 5. Eine Info zu einem Land modifizieren

- **[IP]/[LAND] (Post Request)**
- **Payload: {Info Kategorie:Info}**
- **Returnt:**
  - **Geupdatete Infos zu diesem Land**
- **Wenn ein land spezifiziert wird, das nicht existiert, wird ein neues Land in der Datenbank erstellt**
  - **Beispiel:**
    - **[IP]/Uzbekistan erstellt ein neues Land in der Datenbank**